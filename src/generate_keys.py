import argparse
import asyncio
import pymongo

import motor.motor_asyncio
import uvloop

from key_service.generator import generate
from key_service.settings import load_config

parser = argparse.ArgumentParser(description="Command generate keys")
parser.add_argument('--length', help="key length", default=4)
parser.add_argument("-c", "--config", type=argparse.FileType('r'),
                    help="Path to configuration file")
args = parser.parse_args()
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


def create_collection(config):
    """
    Создаем уникальный индекс по ключу, чтобы не было возможности создать дубли ключей.
    """
    client = pymongo.MongoClient(config['db_host'], config['db_port'])
    db = client.key_service
    index_name = 'key'
    if f'{index_name}_1' not in db.keys.index_information():
        db.keys.create_index(index_name, unique=True)
    client.close()


async def start():
    print('start')
    config = load_config(args.config)
    db_client = motor.motor_asyncio.AsyncIOMotorClient(config['db_host'], config['db_port'])
    db = db_client.key_service
    create_collection(config=config)
    await asyncio.gather(generate(db=db, length=int(args.length)))

    print('stop')


if __name__ == '__main__':
    asyncio.run(start())

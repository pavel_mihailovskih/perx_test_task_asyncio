import motor.motor_asyncio
from aiohttp import web

from key_service.routes import setup_routes


async def create_app(config: dict):
    app = web.Application()
    app['config'] = config
    setup_routes(app)
    app.on_startup.append(on_start)
    return app


async def db_connect(config):
    return motor.motor_asyncio.AsyncIOMotorClient(config['db_host'], config['db_port'])


async def on_start(app):
    config = app['config']
    app['db'] = await db_connect(config)

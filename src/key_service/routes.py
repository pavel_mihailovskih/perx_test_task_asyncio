from key_service.api.views import KeyGiveApi, KeyCheckApi, KeyActivateApi, KeysInfoApi


def setup_routes(app):
    app.router.add_view(path='/api/v1/activate/', handler=KeyActivateApi)
    app.router.add_view(path='/api/v1/give/', handler=KeyGiveApi)
    app.router.add_view(path='/api/v1/check/', handler=KeyCheckApi)
    app.router.add_view(path='/api/v1/info/', handler=KeysInfoApi)

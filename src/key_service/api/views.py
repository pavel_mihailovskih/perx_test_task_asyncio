from aiohttp import web
from aiohttp.web import View

from key_service.api.checks import check_validity_key, check_key_redeem, check_not_found_key, \
    check_request_param_for_activate_key
from key_service.api.key_mongo import KeyMongo
from .serializers import OnlyKeySerializer, KeyStatusSerializer, KeyActivateSerializer


class BaseJsonApi(View):
    serializer_class = None
    key = None
    key_class = KeyMongo

    def __init__(self, *args, **kwargs):
        super(BaseJsonApi, self).__init__(*args, **kwargs)
        self.key = self.key_class(db=self.request.app['db'])

    def get_serializer_class(self):
        assert self.serializer_class is not None, (
                "'%s' should either include a `serializer_class` attribute, "
                "or override the `get_serializer_class()` method."
                % self.__class__.__name__
        )
        return self.serializer_class


class WithCheckQuery:
    def validate_query_params(self):
        """
        Проверка параметров запроса.
        """
        raise NotImplementedError

    def get_key_from_request(self):
        """
        Получаем параметры ключа из запроса
        """
        raise NotImplementedError


class WithCheckInstance:
    def validate_object_instance(self, instance):
        """
        Проверка ключа из БД, на возможность проведения с ней каких либо действий.
        """
        raise NotImplementedError


class KeyGiveApi(BaseJsonApi):
    serializer_class = OnlyKeySerializer

    async def post(self):
        """
        Выдача ключа
        """
        serializer_class = self.get_serializer_class()
        instance = await self.key.give_key()
        return web.json_response(data=serializer_class(instance).data)


class KeyCheckApi(BaseJsonApi, WithCheckInstance, WithCheckQuery):
    serializer_class = KeyStatusSerializer

    def validate_object_instance(self, instance):
        """
        Проверка: ключ найден в БД
        """
        check_not_found_key.check(key=instance)

    def validate_query_params(self, **kwargs):
        """
        Проверка: в запросе присутствует валидный ключ.
        """
        check_validity_key.check(key=kwargs['key'])

    def get_key_from_request(self) -> dict:
        """
        Из запроса получили параметры ключа для поиска в БД
        """
        key_param = self.request.rel_url.query.get('key')
        self.validate_query_params(key=key_param)
        return {'key': key_param}

    async def get_key_instance(self):
        """
        Получили ключ из БД.
        :return:
        """
        key_param = self.get_key_from_request()
        instance = await self.key.get_key_record(condition=key_param)
        self.validate_object_instance(instance=instance)
        return instance

    async def get(self):
        """
        Получение текущего ключа
        """
        instance = await self.get_key_instance()
        serializer_class = self.get_serializer_class()
        return web.json_response(data=serializer_class(instance).data)


class KeyActivateApi(BaseJsonApi, WithCheckInstance, WithCheckQuery):
    serializer_class = KeyActivateSerializer

    def validate_object_instance(self, instance):
        """
        Проверка: ключ найден в БД и может быть активирован
        """
        check_not_found_key.check(key=instance)
        check_key_redeem.check(key=instance)

    async def validate_query_params(self):
        """
        Проверяем на валидность параметры из запроса
        """
        request = self.request
        await check_request_param_for_activate_key.check(request=request)

    async def get_key_from_request(self) -> dict:
        """
        Получаем параметры из запроса.
        :return: словарь для поиска ключа в БД
        """
        await self.validate_query_params()
        data = await self.request.json()
        key_from_request = data.get('key')
        check_validity_key.check(key=key_from_request)
        return {'key': key_from_request}

    async def get_key_instance(self):
        """
        Получаем ключ из БД, проверяем на возможность активации
        """
        key_param = await self.get_key_from_request()
        instance = await self.key.get_key_record(condition=key_param)
        self.validate_object_instance(instance=instance)
        return instance

    async def put(self):
        """
        Активация ключа
        """
        instance = await self.get_key_instance()
        instance = await self.key.activate(instance=instance)
        serializer_class = self.get_serializer_class()
        return web.json_response(data=serializer_class(instance).data)

    async def patch(self):
        return await self.put()


class KeysInfoApi(BaseJsonApi):
    serializer_class = OnlyKeySerializer

    async def get(self):
        """
        Информация о ключах - количество свободных ключей
        """
        return web.json_response(data={"not_issued": await self.key.get_count_not_issued()})

import json

import aiohttp.web_exceptions


class BaseApiError(aiohttp.web_exceptions.HTTPClientError):

    def __init__(self, *args, **kwargs):
        text = kwargs.pop('text', '')
        content_type = kwargs.pop('content-type', 'application/json')
        super(BaseApiError, self).__init__(
            text=json.dumps({
                'msg': text
            }),
            content_type=content_type,
            *args,
            **kwargs
        )


class ApiBadRequestError(BaseApiError):
    status_code = 400


class ApiNotFoundError(BaseApiError):
    status_code = 404


class ApiInternalServerError(BaseApiError):
    status_code = 500

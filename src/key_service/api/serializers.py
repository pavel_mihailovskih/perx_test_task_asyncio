from key_service.api.utils import status_by_id


class BaseSerializer:
    def __init__(self, data):
        self._data = data

    @property
    def data(self):
        raise NotImplementedError()


class OnlyKeySerializer(BaseSerializer):
    @property
    def data(self):
        return {
            'key': self._data['key']
        }


class KeyStatusSerializer(BaseSerializer):
    @property
    def data(self):
        return {
            'status': status_by_id(self._data['status'])
        }


class KeyActivateSerializer(BaseSerializer):
    @property
    def data(self):
        return {
            'key': self._data['key'],
            'status': status_by_id(self._data['status'])
        }

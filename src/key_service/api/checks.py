import collections
import json
import string

from key_service.constants import STATUS_CHOICE_REDEEMED, STATUS_CHOICE_ISSUED, STATUS_CHOICE_NOT_ISSUED
from .exceptions import ApiBadRequestError, ApiNotFoundError, ApiInternalServerError


class BaseCheck:
    """
    Базовый класс для проверки возможности активации ключа
    """
    error_class = ApiBadRequestError

    def get_error_class(self):
        return self.error_class

    def check(self, *args, **kwargs):
        raise NotImplementedError()


class BaseKeyCheck:
    def get_key(self, **kwargs):
        return kwargs['key']


class BaseRequestCheck:
    def get_request(self, **kwargs):
        return kwargs['request']


class CheckKeyStatusIsRedeemed(BaseCheck, BaseKeyCheck):
    """
    Проверяем: статус ключа, и может ли он быть активирован.
    Нельзя повторно активировать ключи.
    """

    def check(self, **kwargs):
        key = self.get_key(**kwargs)
        if key['status'] == STATUS_CHOICE_REDEEMED:
            error_class = self.get_error_class()
            raise error_class(text='Ключ не может быть повторно активирован')

        return True


class CheckKeyStatusIsIssued(BaseCheck, BaseKeyCheck):
    """
    Проверяем: статус ключа, и может ли он быть активирован.
    Активировать можно только выданный ключ STATUS_CHOICE_ISSUED
    """

    def check(self, **kwargs):
        key = self.get_key(**kwargs)
        if key['status'] == STATUS_CHOICE_ISSUED:
            return True

        error_class = self.get_error_class()
        raise error_class(text='Ключ имеет не верный статус, должен быть "Не выдан"')


class CheckKeyStatusIsNotIssued(BaseCheck, BaseKeyCheck):
    """
    Проверяем: статус ключа, и может ли он быть активирован.
    Нельзя активировать не выданный ключ.
    """

    def check(self, **kwargs):
        key = self.get_key(**kwargs)
        if key['status'] == STATUS_CHOICE_NOT_ISSUED:
            error_class = self.get_error_class()
            raise error_class(text='Ключ не выдан, поэтому не может быть активирован.')

        return True


class CheckKeyLength(BaseCheck, BaseKeyCheck):
    """
    Проверяем: длинну ключа, должна быть 4 символа
    """

    def check(self, **kwargs):
        key = self.get_key(**kwargs)
        if len(key) == 4:
            return True

        error_class = self.get_error_class()
        raise error_class(text='Неверная длинна, ключ должен состоять из 4 символов.')


class CheckKeyComposition(BaseCheck, BaseKeyCheck):
    """
    Проверяем: состав ключа, должен состоять из заглавных, строчных символов и чисел [A-Za-z0-9]
    """

    def check(self, **kwargs):
        key = self.get_key(**kwargs)
        counters = collections.Counter()
        error_class = self.get_error_class()

        for i in key:
            if i in string.ascii_uppercase:
                counters['uppercase'] += 1
            elif i in string.ascii_lowercase:
                counters['lowercase'] += 1
            elif i in string.digits:
                counters['digits'] += 1

        if len(counters.keys()) == 3:
            return True

        raise error_class(
            text='Неверный состав ключа, ключ должен состоять из заглавных, строчных символов и чисел.'
        )


class CheckKeyNotFound(BaseCheck, BaseKeyCheck):
    """
    Проверяем: ключ не найден
    """

    error_class = ApiNotFoundError

    def check(self, **kwargs):
        key = self.get_key(**kwargs)
        if not key:
            error_class = self.get_error_class()
            raise error_class(text='Ключ не найден.')

        return True


class CheckRequestBodyExists(BaseCheck, BaseRequestCheck):
    async def check(self, **kwargs):
        """
        Body запроса должен иметь параметры.
        """
        request = self.get_request(**kwargs)
        if not request.body_exists:
            raise ApiInternalServerError(text='Запрос не должен иметь пустой body')

        return True


class CheckRequestBodyIsJson(BaseCheck, BaseRequestCheck):
    async def check(self, **kwargs):
        """
        Body запроса должен конвертироваться в json
        """
        request = self.get_request(**kwargs)
        try:
            json.loads(await request.text())
        except ValueError:
            raise ApiInternalServerError(text='Body запроса должно быть json')

        return True


class CheckKey:
    def __init__(self):
        self._checks = []

    def add_check(self, check):
        self._checks.append(check)

    def check(self, **kwargs) -> bool:
        results = []

        for check in self._checks:
            result = check.check(**kwargs)
            results.append(result)

        return all(results)


class AsyncChecker:
    def __init__(self):
        self._checks = []

    def add_check(self, check):
        self._checks.append(check)

    async def check(self, **kwargs) -> bool:
        results = []

        for check in self._checks:
            result = await check.check(**kwargs)
            results.append(result)

        return all(results)


# Может ли ключ быть погашен
check_key_redeem = CheckKey()
check_key_redeem.add_check(CheckKeyStatusIsRedeemed())
check_key_redeem.add_check(CheckKeyStatusIsNotIssued())
check_key_redeem.add_check(CheckKeyStatusIsIssued())

# Проверка на валидность ключа (4 символа, и состав)
check_validity_key = CheckKey()
check_validity_key.add_check(CheckKeyNotFound())
check_validity_key.add_check(CheckKeyLength())
check_validity_key.add_check(CheckKeyComposition())

# Найден или нет ключ в БД
check_not_found_key = CheckKey()
check_not_found_key.add_check(CheckKeyNotFound())

# Проверка параметров запроса при активации ключа
check_request_param_for_activate_key = AsyncChecker()
check_request_param_for_activate_key.add_check(CheckRequestBodyExists())
check_request_param_for_activate_key.add_check(CheckRequestBodyIsJson())

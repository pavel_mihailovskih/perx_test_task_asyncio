from key_service.constants import STATUS_CHOICES


def status_by_id(status_id):
    """
    Текстовое название статуса ключа
    """
    return next(filter(lambda status_choice: status_choice[0] == status_id, STATUS_CHOICES))[1]

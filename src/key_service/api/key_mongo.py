from key_service.constants import STATUS_CHOICE_NOT_ISSUED, STATUS_CHOICE_ISSUED, STATUS_CHOICE_REDEEMED


class KeyMongo:
    def __init__(self, db):
        self.db = db
        self.db_collection = db.key_service.keys

    async def get_not_issued_count(self):
        """
        Колличество не использованных ключей
        """
        return await self.db_collection.count_documents({"status": STATUS_CHOICE_NOT_ISSUED})

    async def get_key_record(self, condition: dict):
        """
        Получить одну запись
        :param condition: условия поиска
        :return:
        """
        key_instance = await self.db_collection.find_one(condition)
        return key_instance

    async def get_count_not_issued(self):
        """
        Количество не использованных ключей
        :return:
        """
        return await self.db_collection.count_documents({"status": STATUS_CHOICE_NOT_ISSUED})

    async def give_key(self):
        """
        получить случайный свободный ключ (не выданный и не погашенный)
        :return:
        """
        pipeline = [
            {'$sample': {'size': 1}},
            {'$match': {"status": STATUS_CHOICE_NOT_ISSUED}}
        ]
        instance = None
        async for key in self.db_collection.aggregate(pipeline):
            instance = key

        if not instance:
            raise RuntimeError('Нет свободных ключей, все выданы или активированы.')

        await self.db_collection.update_one({'_id': instance['_id']}, {'$set': {'status': STATUS_CHOICE_ISSUED}})
        return instance

    async def activate(self, instance: dict):
        """
        Погасить ключ
        :param instance: ключ из БД ({'key': '1Aaa', 'status': 1})
        :return:
        """
        condition = {'key': instance['key']}
        await self.db_collection.update_one(condition, {'$set': {'status': STATUS_CHOICE_REDEEMED}})
        key_instance = await self.db_collection.find_one(condition)
        return key_instance

import itertools
import string

from .constants import STATUS_CHOICE_NOT_ISSUED


def get_unique_char_set(required_chars, length):
    """
    Список уникальных перестановок надоров символов для генерации ключа
    [('AB', 'ab', '12', 'ab'), ('ab', 'AB', '12', 'ab'), ...... ('12', 'ab', 'AB','ab')]
    """
    items = []
    for part in required_chars:
        for item in itertools.permutations(required_chars + [part], length):
            if item not in items:
                items.append(item)
                yield item


async def generate_key_from_permutation(db_table, permutation):
    """
    Генерация ключей из перестановки символов
    :param db_table: таблица для сохранения ключей в mongo
    :param permutation: ('AB', 'ab', '12', 'ab')
    """
    key_list = []

    print(permutation)
    for key in itertools.product(*permutation):
        key_list.append({"key": ''.join(key), "status": STATUS_CHOICE_NOT_ISSUED})
    await db_table.insert_many(key_list)


async def generate(db, required_chars=None, length=None):
    required_chars = required_chars or [string.ascii_lowercase, string.ascii_uppercase, string.digits]
    length = length or 4
    db_table = db.keys
    unique_char_set = get_unique_char_set(required_chars=required_chars, length=length)

    for item in unique_char_set:
        await generate_key_from_permutation(db_table=db_table, permutation=item)

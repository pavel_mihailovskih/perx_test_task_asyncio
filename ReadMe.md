Описание задачи в файле task.md

---

##### Генерация ключей
docker-compose up -d perx_backend

docker-compose exec perx_backend bash

python generate_keys.py -c local.yaml

или

docker-compose up -d perx_backend

docker-compose exec perx_backend python generate_keys.py -c local.yaml


---

##### Отчет по работе сервиса
*	Выдача уникальных ключей - http://joxi.ru/D2PdGOZip3P8R2?d=1

*	Погашение ключа. 

Помечает ключ как использованный - http://joxi.ru/5mdB5bEckEWOGA?d=1

Повторно погасить ключ нельзя - http://joxi.ru/zAN6GRLUBYPL8A?d=1

Не верная длинна ключа - http://joxi.ru/MAjBZPwc4el3EA?d=1

Не верный состав ключа - http://joxi.ru/DmBZqoGUweakDm?d=1

Отправлено пустое значение - http://joxi.ru/v29GoM1u3eyn9r?d=1

Не валидный json - http://joxi.ru/YmE3GQjs0pyk7A?d=1

Нельзя погасить ключ, если он не был предварительно выдан клиенту - 
http://joxi.ru/bmoJnPwUxB4L8r?d=1

*	Проверка ключа. 

Возвращает информацию информацию о ключе: 

не выдан - http://joxi.ru/V2V3EyXsx6olPA?d=1

выдан - http://joxi.ru/v29GoM1u3eaPWr?d=1

погашен - http://joxi.ru/5mdB5bEckEWOGA?d=1

*	Информация о количестве оставшихся, не выданных ключах - http://joxi.ru/Y2LQBZWU905NwA?d=1
